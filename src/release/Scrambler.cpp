/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Scrambler.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Dillon Flohr
 *			Nawaf Alsudairy
 * @brief   Scrambler implementation.
 *
 * @copyright Jim Daehn, 2017. All rights reserved
 */


#include <sstream>

#include "Scrambler.h"

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {
    Scrambler::Scrambler(const char a[], index beginIndex, index endIndex)
            : begin(beginIndex), end(endIndex) {
        // DONE: Copy the given array into the data member array defined by
        // the given boundaries.
		for (index i = begin; i < end; i++) {
			data[i] = a[i];
		}
    }

    size_t Scrambler::length() const {
        // TODO: Compute me correctly.
        return end;
    }

    std::string Scrambler::scramble() const {
        // DONE: Use reverse() correctly to obtain the data in reverse.
        // You are tasked with determined what the arguments need to be for
        // correct operation. You are free to declare any local variables to
        // make that happen.
		
		//Make a new stringstream
		std::stringstream ss;
		//Send it to reverse
		reverse(data, ss, begin, end);
		//print a string version
		return ss.str();
    }

    std::string Scrambler::toString() const {
        // DONE: Create a std::string representation of the character data
        // stored by this Scrambler.
		std::stringstream ss;
		for (int i = begin; i < end; i++){
			ss << data[i];
		}
		std::string s = ss.str();
		return s;
    }


    Scrambler::~Scrambler() {
        // No op... leave me alone.
    }

    // Private data member implementations
    // Compare/contrast this with writeArrayBackward on page 67.
    void Scrambler::reverse(const char anArray[], std::stringstream& ss,
                            const int first, const int last) const {
		//Take the data and put it in the passed by reference
		//stringstream in reverse.
        if (first < last){
		ss << anArray[last - 1];
		reverse(data, ss, first, last - 1);
		}
    }
}
